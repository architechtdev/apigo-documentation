===============
`IP Whitelist`_
===============

.. _How To Add IP Whitelist?: https://apigo.com/admin/restrictions

What is IP Whitelist?
---------------------

IP addresses that are allowed to make traffic with Endpoints are defined. If the IP address of the incoming request is not described in the IP Whitelist, ApiGo does not allow this traffic. In other words, if the IP Whitelist is defined, ApiGo only allows traffic to the IP addresses in the IP Whitelist. Tenant admin and user with restrictions authority can define IP Whitelist specific to Environment and endpoint.

How To Configure IP Whitelist?
------------------------------

IP Whitelist can be defined in 2 different ways in ApiGo. One of them works by controlling all requests based on the environment. Another way of definition is to define an IP Whitelist specific to the endpoint. When defined in this way, only requests made to that endpoint are subject to IP Whitelist control.

**Tenant Base IP Whitelist Configurations**

1) After logging into ApiGo, you can switch to the restrictions page from the menu. Then click on the IP Whitelist tab.

2) It is defined by entering the IP address in the input field on the screen.

.. figure:: ../docs/images/IPWhitelist/IPWhitelist_environment.png
    :alt: apigo IP Whitelist restriction

**Endpoint Base IP Whitelist Configurations**

1) After logging into ApiGo, you can switch to the endpoints page from the menu.

2) Click the edit button of the endpoint you want to add the IP Whitelist to.

3) On the screen that opens, click the add policy button. Restrictions -> IP Whitelist field is switched. It is saved by filling in the input field.

.. figure:: ../docs/images/IPWhitelist/IPWhitelist_endpoint.png
    :alt: apigo IP Whitelist endpoint

.. figure:: ../docs/images/IPWhitelist/IPWhitelist_policy.png
    :alt: apigo IP Whitelist policy

**Test Your Configuration**

.. figure:: ../docs/images/IPWhitelist/IPWhitelist_postman.png
    :alt: apigo IP Whitelist postman