=======================
`Country Restrictions`_
=======================

.. _How To Add Country Restrictions?: https://apigo.com/admin/restrictions

What is Country Restrictions?
---------------------

Sometimes companies may not want to allow access from certain countries. Country restrictions policy was developed to meet this need. The country that is not allowed make traffic with Endpoints is defined. If the country of the incoming request is described in the Country Restrictions, ApiGo does not allow this traffic. If Country Restrictions are specified, Apigo will only allow traffic between country and endpoint that is not in Country Restrictions. Tenant admin and user with restrictions authority can define Country Restrictions specific to Environment.

How To Configure Country Restrictions?
--------------------------------------

1) After logging into ApiGo, you can switch to the restrictions page from the menu. Then click on the Country Restrictions tab.

2) Add the countries where you want the traffic to be restricted.

.. figure:: ../../docs/images/CountryRestrictions/CountryRestrictions_environement.png
    :alt: apigo Country Restrictions restriction

**Test Your Configuration**

.. figure:: ../../docs/images/CountryRestrictions/CountryRestrictions_postman.png
    :alt: apigo Country Restrictions postman
