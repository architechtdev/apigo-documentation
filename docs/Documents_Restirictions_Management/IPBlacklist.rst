===============
`IP Blacklist`_
===============

.. _How To Add IP Blacklist?: https://apigo.com/admin/restrictions

What is IP Blacklist?
---------------------

IP addresses that are not allowed make traffic with Endpoints are defined. If the ip address of the incoming request is defined in the IP Blacklist, ApiGo does not allow this traffic. That is, if the IP Blacklist is defined, Apigo only allows traffic to IP addresses that are not in the IP Blacklist. Tenant admin and user with restrictions authority can define IP Blacklist specific to Environment and endpoint.

How To Configure IP Blacklist?
------------------------------

IP Blacklist can be defined in 2 different ways in ApiGo. One of them works by controlling all requests based on the environment. Another way of definition is to define an IP Blacklist specific to the endpoint. When defined in this way, only requests made to that endpoint are subject to IP Blacklist control.

**Tenant Base IP Blacklist Configurations**

1) After logging into ApiGo, you can switch to the restrictions page from the menu. Then click on the IP Blacklist tab.

2) It is defined by entering the IP address in the input field on the screen.

.. figure:: ../docs/images/IPBlacklist/IPBlacklist_environment.png
    :alt: apigo IP IPBlacklist restriction

**Endpoint Base IP Blacklist Configurations**

1) After logging into ApiGo, you can switch to the endpoints page from the menu.

2) Click the edit button of the endpoint you want to add the IP Blacklist to.

3) On the screen that opens, click the add policy button. Restrictions -> IP Blacklist field is switched. It is saved by filling in the input field.

.. figure:: ../docs/images/IPWhitelist/IPWhitelist_endpoint.png
    :alt: apigo IP IPBlacklist endpoint

.. figure:: ../docs/images/IPBlacklist/IPBlacklist_policy.png
    :alt: apigo IP IPBlacklist policy

**Test Your Configuration**

.. figure:: ../docs/images/IPBlacklist/IPBlacklist_postman.png
    :alt: apigo IP IPBlacklist postman