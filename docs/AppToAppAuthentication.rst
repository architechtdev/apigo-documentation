=========================
App To App Authentication
=========================

One of the primary objectives of this document is to provide simplification and consistency across “App to App Authentication”. As such, we have defined a core set of details that included design, subject to the scope, and skillset of App to App Authentication provided by TPPs.

Authentication methods – Overview
---------------------------------

The European Banking Authority’s (EBA) notes that "there would appear to currently be three main ways or methods of carrying out the authentication procedure of the end-user through a dedicated interface, and APIs in particular, namely redirection, embedded approaches and decoupled approaches (or a combination thereof). In the cases of redirection and decoupled approaches, end-user’s authentication data are exchanged directly between end-users and banks, as opposed to embedded approaches, in which end-user’s authentication data are exchanged between TPPs and the banks through the interface."

PSD2 requires strong customer authentication to be performed in certain circumstances. The Regulatory Technical Standard (RTS) requires that this application of strong customer authorisation is based on the use of elements, which are categorised as knowledge (something only the user knows), possession (something only the user possesses), and inherence (something the user is). These elements require adequate security features, which include ensuring that they are applied independently, so the breach of any element does not compromise the reliability of the other.

The Open Banking standards specified redirection authentication flows only, and the current bank implementations of redirection are predominantly browser-based, whereby the end-user is redirected from the TPP app or website to the bank website in order to authenticate. It is essential that when redirection is implemented, it also allows the end-user to use their bank's mobile app to authenticate if the end-user uses this method of authentication when accessing their bank channel directly.

The three general principles that apply relating to authentication are:

#. Banks authenticate: End-user needs to go through a strong customer authentication at their bank for a TPP request (i.e., access to information or payment initiation) to be actioned by the bank. 
#. End-Users should have their normal authentication methods available:  A end-user should be able to use the elements they prefer to authenticate with their bank if supported when interacting directly with their bank. 
#. Parity of experience: The experience available to an end-user when authenticating a journey via a TPP should involve no more steps, delay, or friction in the customer journey than the equivalent experience they have with their bank when interacting directly.

`App to App Connector`_
-----------------------

.. _App to App Connector: https://apigo.com/admin/customer-validation

Firebase Authentication integrates tightly with other Firebase services, and it leverages industry standards like OAuth 2.0 and OpenID Connect so that it can be easily integrated with your custom backend.

To provision your Firebase Server Key and Firebase Sender ID, to use in `ApiGo`_, an android mobile app, chrome app or extension, or an Amazon app is a requirement. This is not for websites. Also, you need a Google account to begin work with Firebase. The following comments will be a guide for a connection with your mobile app with ApiGo.

`ApiGo`_ needs to know the identity of a user. Knowing a user's identity allows ApiGo to provide a personalized experience across all of the user's services. A user can use "App to App Settings" as a tool to validate the customer. It includes two different settings to integrate the mobile application with ApiGo.

.. _ApiGo: https://apigo.com

`First Step: Create A Firebase Project`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _First Step: Create A Firebase Project: https://firebase.google.com/

If you already have an FCM project you would like to use with ApiGo; you will need to retrieve your Sender ID and Firebase Cloud Messaging token. You may then skip to Step 2.
Otherwise, visit the Firebase Console and sign in with your Google account.

.. figure:: ../docs/images/apptoappimages/Figure1FirebaseMainPage.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure1FirebaseMainPage.png
    :alt: apigo firebase console

    Figure 1. Firebase Main Page

Click **CREATE NEW PROJECT** or select an existing one below.

.. figure:: ../docs/images/apptoappimages/Figure2FirebaseConsolePage.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure2FirebaseConsolePage.png
    :alt: apigo firebase project

    Figure 2. Firebase Console Page

Enter a project name and press **CREATE PROJECT**.

.. figure:: ../docs/images/apptoappimages/Figure3Pop-UptoCreateaNewProject.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure3Pop-UptoCreateaNewProject.png
    :alt: apigo firebase new project

    Figure 3. Pop-Up to Create a New Project

`Second Step: Getting Your Firebase Cloud Messaging Token And Sender ID`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _Second Step: Getting Your Firebase Cloud Messaging Token And Sender ID: https://console.firebase.google.com/

Click the gear icon in the top left and select Project settings.

Select the **CLOUD MESSAGING** tab.

Save the two values listed under Server key and Sender ID.

.. figure:: ../docs/images/apptoappimages/Figure4ProjectCredentialsDetails.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure4ProjectCredentialsDetails.png
    :alt: apigo firebase project setting

    Figure 4. Project Credentials Details

Third Step: Configure Your App to App Settings on ApiGo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _Third Step: Configure Your App to App Settings on ApiGo: https://apigo.com/admin/customer-validation

In the Customer Validation, select your validation method from the All Adaptors page, then go to App to App Adaptor. Paste your Firebase Server Key, Firebase Sender ID, and Push URL into the fields and click Save.

Done! Application ID, Application Secret, and Scopes are ready to create an App to App Token. Also, with client credential controls, ApiGo uses these details in the requests to add a new device sent by the bank. Client credential settings, ready to copy the clipboard. You can easily reach them anytime you need.

.. figure:: ../docs/images/apptoappimages/Figure5ApptoAppConnector.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure5ApptoAppConnector.png
    :alt: apigo apptoapp Connector

    Figure 5. App to App Connector

User Journey: The End User Identifier
-------------------------------------

A major addition to the Open Banking standards known as “Decoupled” authentication, where typically the end-user uses a separate, secondary device to authenticate with the bank, ApiGo’s Management Portal user. This model allows for a number of innovative solutions and has the added benefit of allowing an end-user to use their mobile phone to authenticate where they are engaging with a TPP through a separate terminal such as a point of sale (POS) device.

There is an example of a Payment Initiation Services (PIS) journey, but the same principles apply for an Account Information Services (AIS) and Card-Based Payment Instrument Issuers (CBPII) journeys. Under the Decoupled standard, the following customer experiences are available:

+----------+------------------------------------------------------+-----------+---------------------+
|  DEVICE  |                         ACTION                       |  PROVIDER |         FROM        |
+==========+======================================================+===========+=====================+
| Device 1 | Select ASPSP, Select Mobile App Available & Enter ID |    TPP    |     TPP - WEB       |
+----------+------------------------------------------------------+-----------+---------------------+
| Device 1 | Payment Information Summary & Proceed                |    TPP    |     TPP - WEB       |
+----------+------------------------------------------------------+-----------+---------------------+
| Device 2 | Push Notification                                    | The bank  | The Bank Mobile App |
+----------+------------------------------------------------------+-----------+---------------------+
| Device 2 | Authentication                                       | The bank  | The Bank Mobile App |
+----------+------------------------------------------------------+-----------+---------------------+
| Device 2 | Payment Confirmation & Original Device Referral      | The bank  | The Bank Mobile App |
+----------+------------------------------------------------------+-----------+---------------------+
| Device 1 | Confirm Transaction                                  |    TPP    |     TPP - WEB       |
+----------+------------------------------------------------------+-----------+---------------------+

* A decoupled authentication flow where the end-user provides a static identifier to the TPP (AISP/PISP/CBPII), which is used by the bank to notify the end-user, such that the bank customer can authenticate using the bank app on a separate device. 
* This enables the end-user to use the same app-based authentication method with the bank they use when accessing the bank's mobile app directly. 
* This model is best suited to TPP apps with good user input options (e.g., a website on PC/laptop).
* The bank must publish the exact type of identifier supported by the bank.

Sequences
---------

.. figure:: ../docs/images/apptoappimages/Figure6EndUserTPPMobileBranchSequences.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure6EndUserTPPMobileBranchSequences.png
    :alt: apigo Sequences

    Figure 6. EndUser- TPP - Mobile Branch Sequences

Wireframes
----------

.. figure:: ../docs/images/apptoappimages/Figure7Wireframes.png
    :align: center
    :figwidth: 600px
    :target: ../docs/images/apptoappimages/Figure7Wireframes.png
    :alt: apigo wireframes

    Figure 7. Wireframes

Requirements
------------

For TPP
~~~~~~~

1. End-User payment Account Selection 
    TPPs must provide end-users at least one of the following options: 
    * enter their Payer's payment Account Identification details 
    * select their Account Identification details (this assumes they have been saved previously)

8. The TPP must confirm the successful confirmation of payment initiation.
    
For ASPSP
~~~~~~~~~

5. After the end-user enters the specified identifier, if the end-user has a bank app, then the bank must notify the end-user through the bank app for authentication purposes without introducing any additional screens. The notification must clearly mention the payment request with the amount and the payee.
    
6. The bank app-based authentication must have no more than the number of steps that the end-user would experience when directly accessing the bank's mobile app (passcode, credentials, etc.).
    
Considerations
--------------

2. The TPP should present the end-user the authentication options supported by the bank, which in turn can be supported by the TPP device/channel (e.g., A TPP kiosk that can only support authentication by bank's mobile app).
    
3.If the TPP and the bank supports the model, then the TPP should request from the end-user identifier, which is supported by their bank.  
    
4.The TPP should make the end-user aware of how this identifier will be used.
    
7.If the end-user is logged off from the bank app, the bank must make the end user aware that they have been logged off and notify them to check back on the originating TPP app. 
    
.. meta::
   :description: Visit our page to learn about authentication from ApiGo app to app, review our documentation and discover the open banking system!