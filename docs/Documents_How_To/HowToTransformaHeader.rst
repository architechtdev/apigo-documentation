=============================
`How To Transform a Header?`_
=============================

.. _How To Define an Header Transformation Policy?: https://apigo.com/admin/endpoints

What is Header Transformation Policy?
-------------------------------------

The Header Transformation policy allows modifying the request's header sent by a TPP on the fly on ApiGo Gateway before hitting the downstream server defined by destination path. Tenant admin can currently add HTTP headers to the current request and the header modification can be executed with the downstream server untouched.

Header Transformation enables you to modify header information before the request passed to your destination path or response is given back to your TPP. The modification can be beneficial to improve the service with authentication processes. A single authentication can be upgraded to multi-user access to the service without modifying it or adding clunky authentication methods to support new users.

For example, TPP calls an endpoint defined on ApiGo, and the tenant admin wants to change the header of an API. Tenant admin has two options. The first one is to modify the API and add a whole user, key, and access management system. ApiGo provides a much more useful second solution to transform the header in place of you.

How It Works?
-------------

1.	By default, all data in a request is passed unchanged from the Endpoint to the Destination Path. The tenant admin can transform the requests and response headers via injecting new keys and values into a request. Use the following path to define a header transformation policy for an endpoint.

`Management Portal`_ -> Endpoints -> Add Endpoint -> Add Policy -> Header Transformation

Choose the transformation type the configurations will be implemented. (Request - Response)

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture1.PNG
    :alt: apigo endpoints page

2.	Tenant admin can add much more than one header transformation source and manage them on the relevant wrapper. There are six types of elements to transform the header.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture2.PNG
    :alt: apigo header transformation policy page

3.	With the header transformation policy, a static string can be added, newly GUID can be generated, an existing header key can be updated.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture3.PNG
    :alt: apigo header transformation policy add source

4.	Also, the header transformation policy provides a useful solution to choose a key from the body, query, or token to modify the header.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture4.PNG
    :alt: apigo header transformation policy add keys

5.	After the configurations have been completed, TPP sends a request to the endpoint with the transformed header. The request has been sent to the gateway with the endpoint defined on Management Portal, and configured call with modified header will be forward to the destination path.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture5.PNG
    :alt: apigo header transformation policy test

6.	The request and response headers directed via ApiGo gateway can be viewed with ApiGo’s Log Management page. Notice the request details sent by TPP, and the request and response headers passing through the ApiGo gateway.

`Management Portal`_ -> Log Management -> `Gateway Logs`_ -> Select the relevant log -> View the Details

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture6.PNG
    :alt: apigo gateway logs detail

7.	When tenant admin analyses the gateway logs, manipulated string, GUID, header key, and other keys from the body, query, or token can be viewed.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture7.PNG
    :alt: apigo gateway logs request headers

8.	On the other hand, all the transformation configurations on the header can be executed on the response header.

.. figure:: ../../docs/images/HeaderTransformation/HeaderTransformationPicture8.PNG
    :alt: apigo gateway logs response headers

.. _Management Portal: https://apigo.com/admin

.. _Gateway Logs: https://apigo.com/admin/gateway-logs

.. meta::
  :description: Click now to learn how to transform a header to the ApiGo Open Banking, to get detailed information and to review our document!