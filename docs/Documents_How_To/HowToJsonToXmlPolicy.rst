=============================
`How To JSON to XML?`_
=============================

.. _How To Define an JSON to XML Policy?: https://apigo.com/admin/endpoints

What is JSON to XML Policy?
-------------------------------------

JSON to XML policy converts the response data of institution backends in JSON format to XML format. This ApiGo policy undertakes the conversion from JSON to XML format and provides data return to clients in XML format.

How It Works?
-------------

1.	Firstly, the endpoint is defined on the Management Portal as in Figure 1. The address of the source system that responds data in JSON format is given to the Target URL part of the Endpoint. The Endpoint configurations are saved and published.

`Management Portal`_ -> Endpoints -> Add Endpoint -> Add Policy -> Json to Xml 

.. figure:: ../../docs/images/JsonToXml/JsontoXmlPicture1.PNG
    :alt: apigo endpoints page

    Figure 1

.. figure:: ../../docs/images/JsonToXml/JsontoXmlPicture2.PNG
    :alt: apigo endpoints page

    Figure 2

2.	We recorded the source API which returns the response value shown in Figure 3. The JSON  response format of the Endpoint we saved, returns to us as converted to XML format shown in Figure 4.

.. figure:: ../../docs/images/JsonToXml/JsontoXmlPicture3.PNG
    :alt: apigo header xml to json policy source page
    
    Figure 3

.. figure:: ../../docs/images/JsonToXml/JsontoXmlPicture4.PNG
    :alt: apigo xml to json postman page

    Figure 4

.. _Management Portal: https://apigo.com/admin

.. _Gateway Logs: https://apigo.com/admin/gateway-logs

.. meta::
  :description: Click now to learn how to json to xml polciy to the ApiGo Open Banking, to get detailed information and to review our document!