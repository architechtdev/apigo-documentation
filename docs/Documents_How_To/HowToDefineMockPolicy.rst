===============================
`How To Define a Mock Policy?`_
===============================

.. _How To Define a Mock Policy?: https://apigo.com/admin/endpoints

What is to Define a Mock Policy?
--------------------------------

To define a mock policy enables ApiGo users to implement and test applications with ApiGo even if the back end is not available to send real responses. The ability to mockup responses can be useful to the user who tries ApiGo the first time. Also, testing with mock data may be useful when the API is designed first, and the backend implementation comes later or, the backend is being developed in parallel.

Tenant admin can define the HTTP Method, authorization type, and version in the first line to the API configuration screen. The admin needs to define an endpoint path configured as a query string and used to reach API by a third-party application in the second one. For more information to define an endpoint, `click here`_. 

In the third line, a destination path that will be used to reach the ApiGo User’s services can be defined by the admin. If mock data is added, there is no need to define a destination path, but otherwise, it must.

.. _click here: https://docs.apigo.com/en/latest/HowToDefineEndpoint.html

Tenant admin can create a fully functional API without writing any line of code to handle consumer calls. The mock policy replaces the default HTTP invoker by putting a mock invoker without an HTTP call between the ApiGo and a backend.

How It Works?
-------------

1) The steps will show how to use ApiGo to return a mocked response with a policy defined on the endpoint configuration screen. Tenant admin can define an endpoint and set a mock policy on the Management Portal.

`Management Portal`_ -> Endpoints -> Add Endpoint -> Add Policy -> Mock

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/HowToDefineMockPolicy/MockPolicyPicture1.PNG
    :alt: apigo endpoints mock

2) The mock policy works with JSON responses, the common format to express data. It has a simple syntax, is human-readable, and can be efficiently parsed in modern languages. Tenant admin can add any JSON syntax with the mock policy and status code, which will be used for responding to the API call. 

.. figure:: ../../docs/images/HowToDefineMockPolicy/MockPolicyPicture2.PNG
    :alt: apigo mock policy page

3) The configurations need to be saved and published on the endpoint definition screen to respond to the API calls with the mock data. The request sent to the gateway will be responded to with mock data and status code defined on the mock policy wrapper.

.. figure:: ../../docs/images/HowToDefineMockPolicy/MockPolicyPicture3.PNG
    :alt: apigo mock policy response

.. meta::
  :description: Click now to learn how to define a mock policy to the ApiGo Open Banking, to get detailed information and to review our document!