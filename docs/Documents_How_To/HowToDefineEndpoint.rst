=============================
`How To Define an Endpoint?`_
=============================

.. _How To Define an Endpoint?: https://apigo.com/admin/endpoints

What is to Define an Endpoint?
------------------------------

It is essential to understand the various aspects and fundamentals of the API and how to manage them to provide the highest user experience level. A crucial thing to understand is what an API endpoint is and why it is needed. Endpoints help to depict the exact location of the resources to be accessed by API and play a vital role in ensuring that the application which is interacting with the API is functioning correctly. 

In the open banking ecosystem, an API endpoint refers to touchpoints of the communication between TPP and ASPSP. An API endpoint is basically a word for a URL of account information or payment initiation services.

As more financial institutions are starting to use open banking APIs to aid in transferring critical data, transactions, and processes, it has become vitally imperative to understand the various aspects that makeup API. Making sure that the communication touchpoints between TPP and ASPSP are robust is crucial to open banking API success. 

How It Works?
-------------

1) Tenant admin can define endpoints to address Account Information and Payment Initiation Services (AIS and PIS) on Management Portal. They can be arranged as ready to use for Third-party Providers (TPP) and help the third-party application's proper functioning.

Management Portal -> Endpoints -> Add Endpoint

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture1.PNG
    :alt: apigo endpoint list

2) After click on “Add Endpoint”, a content box will be appeared to define an endpoint. In the first line, tenant admin can define the HTTP method (ApiGo supports GET, POST, PUT, DELETE methods), Authorization Type (ApiGo supports Client Credentials and Authorization Code), and Version (ApiGo provides API versioning function).

To create an endpoint, the tenant admin needs to define an endpoint path that will be configured as a query string and used to reach API by a third-party application. Also, a destination path that will be used to reach the ApiGo User’s services can be defined by tenant admin to define an API on the developer portal. If mock data is added, there is no need to define a destination path, but otherwise, it is a must.

Product can be defined as the scope of the endpoint. Before, tenant admin needs to set the scopes (Management Portal -> Configurations -> Scope Settings). Scope Name is the key value and scope display name can be explained as product in the endpoint definition screen.

The status of the endpoint can be managed by the tenant admin to set the activation of the API. Also, the endpoint can be configured as a static endpoint. API design can be modified with policies whose list will be appeared after clicking on “Add Policy” includes. After the definition has been completed to design an endpoint the changes must be saved and published in a new version of the collection to activate them on the gateway.

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture2.PNG
    :alt: apigo add endpoint

3.a) To define an endpoint with GET method and client credential token, the API design will be like the following image.

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture3.PNG
    :alt: apigo add policy

3.b) To define an endpoint with POST method and authorization code, the API design will be like the following image.

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture4.PNG
    :alt: apigo endpoint method

4.a) Getting a new access token with Client Credential grant type can be seen in the following image via Postman. For more information on how to get a client credential token, please click on `here`_.

.. _here: https://docs.apigo.com/en/latest/HowToGetaClientCredentialToken.html

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture5.PNG
    :alt: apigo postman token

4.b) Getting a new access token with Authorization Code grant type can be seen in the following image via Postman. 

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture6.PNG
    :alt: apigo authorization token

After requesting an access token with the Authorization Code, a 2-factor authentication or any other authentication process can be executed. And once ApiGo is notified that the authentication process has been successful, ApiGo will ask the PSU if it allows the scopes the TPP transmitted.

5.a) When the request will be sent to the gateway with the client credential token, the call will be responded to successfully like the following image.

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture7.PNG
    :alt: apigo postman request

5.b) When the request will be sent to the gateway with the authorization code, the call will be responded to successfully like the following image.

.. figure:: ../../docs/images/ToDefineEndpoint/ToDefineEndpointPicture8.PNG
    :alt: apigo postman response

.. meta::
  :description: Click now to learn how to define an endpoint to the ApiGo Open Banking, to get detailed information and to review our document!