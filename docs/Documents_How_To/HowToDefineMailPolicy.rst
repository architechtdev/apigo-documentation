===============================
`How To Define a Mail Policy?`_
===============================

.. _How To Define Mail Policy?: https://apigo.com/admin

What is Mail Policy?
--------------------

ApiGo enables the users to receive an email when an API is called using the Simple Mail Transfer Protocol (SMTP). The Mail Policy allows ApiGo users to instruct the gateway to deliver a pre-configured email message whenever their developers call for an endpoint.

The policy helps the tenant admins to control the endpoints via email. In ApiGo Management Portal, tenant admin can define mail policies and forward them to various receivers. The admin can define mail policies for specific email addresses with specialized messages.

How It Works?
-------------

1.	Tenant admin may use the below path to complete the necessary settings if the configurations has not been adjusted before. Management Portal provides an easy and quick solution to inform ApiGo users about any responded API call.

Management Portal -> `Configurations`_ -> SMTP Settings

2.	ApiGo users can be notified with an e-mail when the gateway responds to a TPP request. A mail policy can be defined for any API in the endpoint definition screen. 

Management Portal -> `Endpoints`_ -> Add Endpoint -> Add Policy -> Mail Policy

.. figure:: ../../docs/images/MailPolicy/MailPolicyPicture1.PNG
    :alt: apigo mail policy

3.	Tenant admin can add a new e-mail address to inform about the responded API call. The informative message can send to more than one receiver. With an add email button, tenant admin may add new email addresses. Also, the message subject and body can be configured on the policy wrapper.

.. figure:: ../../docs/images/MailPolicy/MailPolicyPicture2.PNG
    :alt: apigo mail policy view

4.	TPP sends a request to the gateway, and the API call is responded to successfully. In the meantime, an e-mail is sent to the defined email addresses with the message subject and body, which is entered via Management Portal’s Policy Screen.

.. figure:: ../../docs/images/MailPolicy/MailPolicyPicture3.PNG
    :alt: apigo mail policy postman response

5.	The informative mail is sent to the relevant email addresses and the SMTP settings need to be already configured to complete the process successfully.  

.. figure:: ../../docs/images/MailPolicy/MailPolicyPicture4.PNG
    :alt: apigo mail policy sended mail

.. _Endpoints: https://apigo.com/admin/endpoints

.. _Configurations: https://apigo.com/admin/smtp-settings

.. meta::
  :description: Click now to learn how to define a mail policy to the ApiGo Open Banking, to get detailed information and to review our document!