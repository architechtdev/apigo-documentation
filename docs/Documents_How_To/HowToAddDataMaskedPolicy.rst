===============================
`How To Add Data Mask Policy?`_
===============================

.. _How To Add Data Mask Policy?: https://apigo.com/admin/endpoints

What is Data Mask Policy?
-------------------------

Data Masking enables entire copies or subsets of application data to be extracted and disguised from the bank services. Data Masking Policy provides a privacy solution for ApiGo users. With this policy, tenant admin can hide the sensitive customer data when supplying the developers with the bank services. ApiGo as a connection point between the bank and the developer transfer the sensitive data directly and logged the action with masked descriptions. So, ApiGo users save their data privacy and do not need to share them with the software service provider.

How It Works?
-------------

1) ApiGo users can empower their privacy of APIs with Data Mask Policy when defining an endpoint in Management Portal. Tenant admin may want to mask the bank’s sensitive data when sharing the services powered by ApiGo for a variety of business needs. Data Mask policy is in General category and can be executed for the environments created with both Berlin Group and UK Open Banking Standards. Selected endpoint’s HTTP Method must be POST or PUT.

`Management Portal`_ -> Endpoints -> Selected Endpoint -> Add Policy -> Masked Fields

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/AddDataMaskPolicy/AddDataMaskPolicyPicture1.png
    :alt: apigo add data mask policy

2) After clicking on Masked Fields to add the policy, the tenant admin must input the data model of the related endpoint with a valid JSON format. All available fields will be brought on the data model with clicked on the “Get Fields” button. The selectable fields are listed and mask exception messages can be updated.

.. figure:: ../../docs/images/AddDataMaskPolicy/AddDataMaskPolicyPicture2_v1.png
    :alt: apigo data mask policy

3) Tenant admin clicks on “Save” button to implement the policy for the selected endpoint and the changes must be published to respond the related request with masked data. The masked data can be seen in Berlin Group Standards as Payment Initiation and Account Consent and UK Open Banking Standards as only Payment Initiation details.

4) In example, when TPP sends a request to the gateway with the selected endpoint, the response message will be with masked Debtor Account IBAN and Debtor Account Currency. The difference between the details shows how the data mask policy changes the response messages. To check the action details, please visit the PIS & Account Consent menu for the environment created with Berlin Group Standards and Payment Initiation menu for the environment created with UK Open Banking Standards.

Management Portal -> PIS & Account Consent -> Payment Initiation Detail 

.. _PIS & Account Consent: https://apigo.com/admin/payment-initiations

.. figure:: ../../docs/images/AddDataMaskPolicy/AddDataMaskPolicyPicture3_v1.png
    :alt: apigo data mask policy view

**- Payment Initiation without Data Mask Policy**

.. figure:: ../../docs/images/AddDataMaskPolicy/AddDataMaskPolicyPicture4_v1.png
    :alt: apigo data mask policy detail

**- Payment Initiation with Data Mask Policy**

.. meta::
  :description: Click now to learn how to add data mask policy to the ApiGo Open Banking, to get detailed information and to review our document!