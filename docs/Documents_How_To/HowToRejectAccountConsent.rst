=================================
`How To Reject Account Consent?`_
=================================

.. _How To Reject Account Consent?: https://apigo.com/admin

What is to Reject Account Consent?
----------------------------------

Consent can be revoked by two types of roles, which include PSU and ASPSP. If the PSU revokes the consent to data access with the AISP, the AISP will not be responded with consent_id and the process will not be continued to access AIS. To inform about the current status of the consent is done by making a call to the gateway for the account-access-consent resource. Before calling the API, the AISP must have an access token issued by the ASPSP using a valid eIDAS certificate or a client credentials grant.

**The consent is rejected by PSU**

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent1.png
    :alt: apigo consent rejected PSU

If the ASPSP asks to revoke the consent to data access with AISP, ApiGo offers a solution on Management Portal to manage account consents. When a PSU trigger TPP to delete an account consent, the DELETE /account-access-consents call allows an AISP to delete a previously created account-access-consent (whether it is currently authorized or not). The PSU may want to remove their consent via the AISP instead of revoking authorization with the ASPSP. This process allows the PSU to revoke consent with the AISP via ASPSP.  

**The consent is rejected by ASPSP**

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent2.png
    :alt: apigo consent rejected ASPSP

How It Works?
-------------

1) When PSU informs to ASPSP to delete an account consent or tenant admin call for it, the admin can be managed the process manually by using Account Consent Management on Management Portal to revoke the related consent. 

The path for the environment created based on Berlin Group Standard

Management Portal -> PIS & Account Consents -> `Account Consents`_ -> The related Account Consent

.. _PIS & Account Consents: https://apigo.com/admin/account-consent-list

The path for the environment created based on UK Open Banking Standard

Management Portal -> Account Consents -> The related Account Consent

.. _Account Consents: https://apigo.com/admin/consents

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent3.PNG
    :alt: apigo account consents page

2) The filters button can be used for filtering the account consents by status, TPP name, and username. The filter button is displayed within the column header and can be clicked to activate filter dropdown lists. Such lists enable tenant admins to specify data filtering conditions.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent4.PNG
    :alt: apigo account consent filter

3) TPP sent a POST request to take consent for Account Service Information. When PSU initiates the consent management process to get account information, the status of the account consent and response message structure will be updated depending on the standard which is selected business model to shape the environment.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent5.PNG
    :alt: apigo account consent response

4) When a TPP sent a consent request to reach AIS, the status will be updated “received” by the gateway. And the current status of the consent can be viewed on Management Portal. After the consent request sent to the gateway, PSU will be informed with a confirmation message (ex. a push notification via ASPSP’s mobile branch). To reach much more information on the consent creation date, status of the consent, PSU name, and TPP name, click on the “more” icon.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent6.PNG
    :alt: apigo account consent edit

5) Consent data which includes balance and transaction information standardized with Open Banking Standards on the body of the request has been sent to the gateway by TPP. There is a revoke button available to reject the received consent.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent7.PNG
    :alt: apigo account consent revoke

6) The status of the consent will be updated with “rejected by tenant admin”. The action time can be viewed on the Account Consent page.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent8.PNG
    :alt: apigo account consent rejected

7) When we checked the status of the consent on the gateway, the response message will be returned with 403 Forbidden status code. Because of the revoke, the consent cannot be found among accessible consents on the gateway.

.. figure:: ../../docs/images/ToRejectAccountConsent/ToRejectAccountConsent9.PNG
    :alt: apigo account consent response not found

.. meta::
  :description: Click now to learn how to modify global rate limit to the ApiGo Open Banking, to get detailed information and to review our document!