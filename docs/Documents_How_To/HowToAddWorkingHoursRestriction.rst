=================================
`How To Restrict Working Hours?`_
=================================

.. _How To Restrict Working Hours?: https://apigo.com/admin/working-hours

What is Working Hours Restriction?
----------------------------------

Working hours can be defined on ApiGo Management Portal to restrict the API calls that came to ApiGo user’s gateway out of the working hours. Tenant admin can configure a restriction, so users are allowed access to the internal network at specific times, such as during normal working hours. When users try to log on at a different time, login is denied.

Limited availability to hour/day approval allows the tenant admin to restrict service access by hour/day range. When a TPP request is sent to the gateway which is configured with a working hours restriction, hour and/or day restrictions are checked before allowing the message to progress.

How It Works?
-------------

1) Exceptions to the gateway availability are provided can be defined with Working Hours Restrictions.  The tenant admin can set the time interval at which the gateway will respond to the TPP requests. 

`Management Portal`_ -> Restrictions -> Working Hours

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/WorkingHours/WorkingHoursPicture1.PNG
    :alt: apigo working hours

2) There are configuration settings for each day of the week. If ApiGo does not find a schedule, it uses the default hours configuration as working all day. By default, Gateway operates all-day, so to customize the start and end hours of any day, click the checkbox to select a time range. Also, the time zone can be determined for the current location of open banking transactions.

.. figure:: ../../docs/images/WorkingHours/WorkingHoursPicture2.PNG
    :alt: apigo working hour selected

3) When the TPP sends the request to the gateway within the available time interval configured on the Management Portal, the response will be returned successfully. The working hours control triggered by the service request sent by TPP will check that the incoming request is within the specified hours and will not prevent the incoming request from being answered.

.. figure:: ../../docs/images/WorkingHours/WorkingHoursPicture3.PNG
    :alt: apigo working hours postman

4) When TPP send the request to the gateway within the unavailable time interval configured on Management Portal, the response will be return as 403 Forbidden HTTP Status Code. Also, there is a message which explains why the request cannot be responded by the gateway.

.. figure:: ../../docs/images/WorkingHours/WorkingHoursPicture4.PNG
    :alt: apigo working hours response

.. meta::
  :description: Click now to learn how to restrict working hours to the ApiGo Open Banking, to get detailed information and to review our document!