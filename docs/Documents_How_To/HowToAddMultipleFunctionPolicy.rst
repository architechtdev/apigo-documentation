=======================================
`How To Add Multiple Function Policy?`_
=======================================

.. _How To Add Multiple Function Policy?: https://apigo.com/admin/endpoints

What is Multiple Function Policy?
---------------------------------

ApiGo users may have offered similar functional endpoints with different service and product types. Developers can use these endpoints running on the gateway with different paths. This policy can be used to define one or more destination paths by specifying sources so that paths under the defined policy can be available. The parameters defined on the destination path can be managed dynamically. So, the similar objected endpoints do not need to create again and again. It can be used to reduce the number of endpoints by combining them into a single endpoint.

How It Works?
-------------

1) Multiple Functions Policy is a specification includes more than one destination paths for endpoints. The policy can be reached in the general category. Only one Multiple Functions Policy can be added for an Endpoint. 

`Management Portal`_ -> Endpoints -> Endpoint Selection -> Add Policy -> Multiple Functions

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/AddMultipleFunctionPolicy/AddMultipleFunctionPolicyPicture1.png
    :alt: apigo multiple Function policy

2) Click on “Add Path” to make more destination paths to reach. Up to five destination addresses can be added. The endpoint path can include payment services and products. There is no need to create an endpoint for every service and product. This control can execute by Gateway when the services are provided by ApiGo users. The relevant service can be added with Multiple Function Policy. To add a new destination path provides to reach more than one source.

Endpoint Path Example: /api/v1/{payment-service}/{payment-product}

.. figure:: ../../docs/images/AddMultipleFunctionPolicy/AddMultipleFunctionPolicyPicture2_v1.png
    :alt: apigo multiple Function policy view

3) Two type of protocols include HTTP and HTTPS are available to use for any destination point.  Transfer Header, Transfer Body (this feature can be selected for methods other than GET) can be added to any endpoint to send the request to Gateway with a header, a body, or both. When a request with following an endpoint comes to the Gateway, the control will be executed and the response which is provided by related service is sent back to the developer.

Destination Path Example 1: /api/v1/payments/sepa-credit-transfers

Destination Path Example 2: /api/v1/bulkpayments/instant-sepa-credit-transfers

Destination Path Example 3: /api/v1/periodicpayments/target-2-payments

Destination Path Example 4: /api/v1/periodicpayments/cross-border-credit-transfers

.. figure:: ../../docs/images/AddMultipleFunctionPolicy/AddMultipleFunctionPolicyPicture3_v1.png
    :alt: apigo multiple Function policy page

4) Destination Path in the Endpoint definition screen does not need to fill if Multiple Function policy has been defined. Finally, save the changes in the policy and the endpoint and publish the version to start to use Multiple Function Policy.

.. meta::
  :description: Click now to learn how to add multiple function policy to the ApiGo Open Banking, to get detailed information and to review our document!