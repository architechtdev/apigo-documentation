=============================
`How To Add an Environment?`_
=============================

.. _How To Add an Environment?: https://apigo.com/admin/environment

.. raw:: html

    <div style="text-align: center; margin-bottom: 2em;">
    <iframe src="https://player.vimeo.com/video/412642380" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>

To add a new environment, you need to log in to the management portal. 

If you are entering for the first time to `ApiGo`_ , you will automatically encounter Add Environment page.

.. _ApiGo: https://apigo.com/

.. figure:: ../../docs/images/HowToAddEnvironment0.png
    :alt: apigo add environment

The picture below shows the page you will encounter.

On this page first, choose your business model that which standard do you want to follow. The environment name is a short description of your new environment. Dev, test, props, etc.

Domain, defined your environment's physical address. For example; dev.00003.apigo.com 

The last step is production’s switch. If you want the manage your production features turn the switch in the direction of yes. When this happens, a new switch will appear below.

.. figure:: ../../docs/images/HowToAddEnvironment1.png
    :alt: apigo add environment button

If an environment set as production and select “Application Approve Workflow” is enabled,  When a developer creates an Application on developer portal,  this application is not working without admin approved it. This Production switch can be changed later.

When click to Add Environment button, a new environment should be created and your page turns automatically new environment dashboard.

.. figure:: ../../docs/images/HowToAddEnvironment2.png
    :alt: apigo select environment

How To Delete Environment
-------------------------

To delete the environment you have created, you have to work with the environment which you want to delete.

You should select which environment you want to work, only the selected environment can be deleted. The picture below shows the screen when we press the selected environment button.

.. figure:: ../../docs/images/EnvironmentList.png
    :alt: apigo environment list

To delete an environment, at least 2 environments must be created. 

After that click Environment from the menu on the left side of the management portal.

.. figure:: ../../docs/images/EnvironmentDetailsToDelete.png
    :alt: apigo environment details to delete

When you click on you will get to the details of the environment, you need to click on the edit button on this page.

After that you will encounter update environment pag

.. figure:: ../../docs/images/DeleteIcon.png
    :alt: apigo environment delete icon

You need click the trash icon.

.. figure:: ../../docs/images/DeleteConfirmation.png
    :alt: apigo environment delete Confirmation

After that you will encounter Confirmation page. This page has been made to prevent accidental deletion. In order to delete the environment, the name of the environment should be written in the text box below.

.. meta::
  :description: Click now to learn how to add an environment to the ApiGo Open Banking, to get detailed information and to review our document!