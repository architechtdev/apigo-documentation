=====================================
`How To Define a Rate Limit Policy?`_
=====================================

.. _How To Define Rate Limit Policy?: https://apigo.com/admin

What is Rate Limit Policy?
--------------------------

Managing app traffic capacity within limits is critical for the sustainable performance and availability of developers' apps. ApiGo’s rate limit policy can be configured for every endpoint by a tenant admin to accomplish to provide better service quality for TPPs. The API call can be rejected when requests exceed the limit. Making high volume API calls by developers can be limited for any API with ApiGo's endpoint policies. The tenant admin can arrange a limit for an endpoint to ensure the quality of service. It is important to ensure that apps do not consume more resources than permitted.  

How It Works?
-------------

1.	Tenant admin can configure the rate limit of any endpoint with the rate limit policy on the Management Portal. To reach the submenu and add a rate limit for an endpoint, use the following path. Up to how many requests can be sent to the gateway, the waiting limit can be configured with the policy. There is an updating and deleting option whenever the admin needs it.

Management Portal -> `Endpoints`_  -> Add Endpoint -> Add Policy -> Rate Limit

.. figure:: ../../docs/images/RateLimitPolicy/RateLimitPolicyPicture1.PNG
    :alt: apigo endpoints rate limit

2.	The rate limit policy provides tenant admin to configure how many requests will cause to error, the delay between retries with Request Wait Time, and the message used to inform the request owner. After the tenant admin completes any endpoint configurations with Rate Limit Policy, the admin needs to publish the configurations to make the changes real.

.. figure:: ../../docs/images/RateLimitPolicy/RateLimitPolicyPicture2.PNG
    :alt: apigo rate limit policy page

3.	The rate limit policy can be used to prevent sudden traffic bursts caused to disrupt service. If the app triggers the rate limit, TPP needs to refrain from making additional requests until the appropriate amount of time has elapsed. Otherwise, the endpoint will be available under the limitations. 

.. figure:: ../../docs/images/RateLimitPolicy/RateLimitPolicyPicture3.PNG
    :alt: apigo postman response

4.	When a request has been sent to reach an endpoint including the rate limit policy, the following response will be returned with the HTTP 429 Status code if the rate has been exceeded.

.. figure:: ../../docs/images/RateLimitPolicy/RateLimitPolicyPicture4.PNG
    :alt: apigo rate limit response

.. _Endpoints: https://apigo.com/admin/endpoints

.. meta::
  :description: Click now to learn how to define a rate limit policy to the ApiGo Open Banking, to get detailed information and to review our document!