==================================
`How To Export/Import Endpoints?`_
==================================

.. _How To Export/Import Endpoints?: https://apigo.com/admin/endpoints

What is to Export/Import Endpoints?
-----------------------------------

The export statement is used when creating JSON files to export live bindings to functions, objects, or primitive values from the platform so they can be used by other programs with the import statement. Bindings that are exported can still be modified locally; when imported, although they can be read by the importing platform the value updates whenever it is updated by the exporting platform. The import statement is used to import live bindings that are exported by another version. Bindings imported are called live bindings because they are updated by the module that exported the binding. The export settings option is also convenient for the admins who want to use their preferred configuration on multiple environments, they can easily import a .json file to transfer these settings.

How It Works?
-------------

1) Tenant admin manages multiple versions of API collections created in ApiGo. Defined endpoints’ version information can be viewed on the Endpoints menu.  The admin can export endpoints from an environment to import another on Management Portal via using the endpoint collection’s versioning function. 

`Management Portal`_ -> Endpoints -> Version Information 

.. _Management Portal: https://apigo.com/admin

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture1.PNG
    :alt: apigo endpoints version

2) When a change is done on the Endpoint page to the defined endpoint, the changes need to be published on Management Portal. To add a new endpoint version to use the export/import function, the publishing process must be done. On the other hand, when an environment created with a business model, it will provide an endpoint collection and version that is compliant with the selected standard.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture2.PNG
    :alt: apigo publish version

3) Importing and exporting of endpoint collections is useful if the tenant admin needs to back up endpoints defined on Management Portal for use at a later time. With the drawer that opens, tenant admin can export an endpoint collection as a JSON template.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture3.PNG
    :alt: apigo endpoints export version page

4) Tenant admin can export the settings to a file and then import the file later to apply the settings to another session. Endpoint History drawer includes the list of versions. Tenant admin can select which version will be restored to use on Management Portal. Also, the selected version can be exported to use later.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture4.PNG
    :alt: apigo endpoints select version

5) Individual endpoints from an environment can be exported to a .json file. The admin can copy the full and partial endpoint collection from one environment to another. After click on the selected version, Endpoint list of the selected version will be available on the drawer. Tenant admin can select all endpoints or click on the checkbox to specify which endpoint will be exported. After the selection, endpoint list is ready to export.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture5.PNG
    :alt: apigo selected endpoints

6) With exporting all selected endpoints, a JSON file has been created to use later. Tenant admin can import the file on which the environment is desired.

Management Portal -> Endpoints -> Version Information -> Import Icon

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture6.PNG
    :alt: apigo import endpoints page

7) To import a JSON file which is created with ApiGo’s Endpoint exporting function, follow the path to reach the endpoint importing page and select the desired endpoints on the opened drawer. After the selection of desired endpoints to import, click on the button which is on the header of the drawer.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture7.PNG
    :alt: apigo selected endpoints

8) The selected endpoints will be seen on Endpoint Defined Screen. With publishing the changes, the imported endpoints will be available on gateway to response TPPs requests.

.. figure:: ../../docs/images/ToExportImportEndpoints/ToExportImportEndpointsPicture8.PNG
    :alt: apigo endpoints page

.. meta::
  :description: Click now to learn how to export/import endpoints to the ApiGo Open Banking, to get detailed information and to review our document!