=================
`Configurations`_
=================

.. _Configurations: https://apigo.com/admin/site-settings

.. note::

    Configurations are settings function to the users. They are able to manage their developer portal and arrange the appearance of it.

Site Settings
-------------

The site settings let the user manage the developer portal website’s external view.

.. image:: ../docs/images/SiteSettings_Images2.png
    :alt: apigo site settings

Appearance-Site Logo
~~~~~~~~~~~~~~~~~~~~

The logo of the developer portal can be adjusted. “Click to upload” button provides to add any image to shape your developer portal website. You can save or delete your image and select another.

.. image:: ../docs/images/SiteLogo.png
    :width: 425
    :alt: apigo appearance

Appearance-Site Header
~~~~~~~~~~~~~~~~~~~~~~

.. image:: ../docs/images/Header.png
    :width: 425
    :alt: apigo site header

The background of the header can be changed. Select your favorite color and click on it. When you save it, the developer portal will be changed.

.. image:: ../docs/images/Header2.png
    :width: 600
    :alt: apigo background color

Home Swiper
~~~~~~~~~~~

.. image:: ../docs/images/HomeSwiper.png
    :alt: apigo home swiper


This function can be used to manage swipers on the main page of the developer portal website. To add a new slide, click on the “Add New” button. You can add card details in the slides. To download an image to the slide, click on to upload and select it. You can make the slide visible or invisible. Add a title and card description to the selected slide.

.. image:: ../docs/images/HomeSwiper2.png
    :width: 600
    :alt: apigo home swiper view

Home Card
~~~~~~~~~

.. image:: ../docs/images/HomeCard.png
    :width: 425
    :alt: apigo home cards

The function manages the textboxes on the main page of the developer portal. You can add a title and an icon for your home card. Also, a description can be added.

.. image:: ../docs/images/HomeCard2.png
    :width: 600
    :alt: apigo home cards view

API Products
~~~~~~~~~~~~

.. image:: ../docs/images/APIProduct.png
    :width: 1200
    :alt: apigo API products
    
This function is to manage your API products, which are served for developers. You can add a title, link, image, and description to customize them.

.. image:: ../docs/images/APIProduct2.png
    :width: 1200
    :alt: apigo API products view

`SMTP Settings`_
-----------------

.. _SMTP Settings: https://apigo.com/admin/smtp-settings

.. note::

    SMTP is part of the application layer of the TCP/IP protocol. Using a process called "store and forward," SMTP moves your email on and across networks. It works closely with something called the Mail Transfer Agent (MTA) to send your communication to the right computer and email inbox. 

.. image:: ../docs/images/SMTP.png
    :width: 600
    :alt: apigo smtp settings

You can identify the SMTP server name, username, and password. Also, add a port, mail address, and security certificate require information. After saving SMTP settings, you can update or test it. 

.. image:: ../docs/images/SMTPSettings_Images2.png
    :alt: apigo smtp mail

`reCAPTCHA Settings`_
---------------------

.. _reCAPTCHA Settings: https://apigo.com/admin/recaptcha-settings

.. note::

    reCAPTCHA is a free service from Google that helps protect websites from spam and abuse. A “CAPTCHA” is a turing test to tell humans and bots apart. It is easy for humans to solve, but hard for “bots” and other malicious software to figure out.

.. image:: ../docs/images/reCAPTCHASettings_Image.png
    :alt: apigo reCAPTCHA setting

To fill in the client and private key, you have to receive inputs from your Google account. After you take these keys, you need to paste them to relevant blanks. 

.. image:: ../docs/images/GooglereCAPTCHA.png
    :alt: apigo google reCAPTCHA

By adding reCAPTCHA to the site, you can block automated software while helping your welcome users to enter with ease. The generated reCAPTCHA will be available in the developer portal.

.. image:: ../docs/images/GooglereCAPTCHA2.png
    :alt: apigo reCAPTCHA register

`Customer Validation`_
----------------------

.. _Customer Validation: https://apigo.com/admin/customer-validation

This function is used to authenticate bank users with ApiGo. There are four fundamentals for configuring validation. These are LDAP, Rest, External Identity Server, and Bank Login.

To add an adapter setting for the environment, click on “Add New” bottom. You will see a list of adaptors on the left side of the page.

.. image:: ../docs/images/CustomerValidation.png
    :alt: apigo customer validation

LDAP Settings
~~~~~~~~~~~~~

* Enter the name of the server where LDAP is hosted, 
* Enter the LDAP server port. The default port is 636.
* Enter the username for logging to the LDAP server. You can use a regular user.
* Enter the password for logging to the LDAP server.
* The Base Distinguished Name for your LDAP directory. For a commercial organization, it may look something like: 'dc=companynamehere,dc=com'
* Determine the search criteria for user logins. By default, users log in using their email address. If you have changed this setting, you must modify the search filter here. For example, if you changed the authentication method to use the screen name, you would modify the search filter so that it can match the entered login name: ' (cn=@screen_name@) '

.. image:: ../docs/images/LDAP.png
    :alt: apigo LDAP setting

Rest
~~~~

* Enter Rest Service Url.
* Optionally, enter the Public Key.

.. image:: ../docs/images/Rest.png
    :alt: apigo rest

External Identity Server
~~~~~~~~~~~~~~~~~~~~~~~~

* Enter your identity server URL
* Enter the Client Id
* Enter the Client Secret
* You need to enter a scope
* You need to enter response type
* Enter the Redirection Uri of the certificate
* You need to select implicit flow type

.. image:: ../docs/images/ExternalIdentityServer.png
    :alt: apigo external identity server

Bank Login
~~~~~~~~~~

You can enter your bank URL in relevant blank.

.. image:: ../docs/images/BankLogin.png
    :alt: apigo bank login

`Export The Settings`_
----------------------

.. _Export The Settings: https://apigo.com/admin/import-export

You can download or upload your environment settings on this page. This function can be used to transfer configuration settings. 

.. image:: ../docs/images/Export-Import.png
    :alt: apigo export import

You can select the items you want to export. To download the configuration settings, click on the “Export” button. The downloaded document will be available to import it.

.. image:: ../docs/images/Export.png
    :alt: apigo export

Import The Settings
-------------------

To upload the configuration settings, click on the “Import” button. 

.. image:: ../docs/images/Import.png
    :alt: apigo import file

Your document will be downloaded. To upload it, click on the “Import” button. You will welcome with a page to import. Select an import file in the page. And, you can specify your import selection. Finally, click on “Next” and change your environment settings.

.. image:: ../docs/images/Import2.png
    :alt: apigo import

.. meta::
   :description: Visit our page to learn about ApiGo configurations, review our document and discover the privileges of the ApiGo Open Banking platform!