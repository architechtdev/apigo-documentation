===============
`Environments`_
===============

.. _Environments: https://apigo.com/admin/environment

You can manage the development, test or production services by creating more than one environment in ApiGo. You do not need to open a separate ApiGo account or set up on-premise for each of these. This document is explains how to change environment in ApiGo. How to create and update a new environment will be explained.

.. image:: ../docs/images/ManageEnvironment_Images.png
  :width: 425
  :alt: apigo environment 

.. _main page: https://apigo.com/admin/

Add a New Environment
~~~~~~~~~~~~~~~~~~~~~

To add an environment, you can use this page. Click on the “Add Environment” button and go back to the addition of an environment page. With the search line, you can find which environment will you work in.

.. image:: ../docs/images/AddanEnvironment_Images.png
  :width: 425
  :alt: apigo add environment

To edit the existing environment, click on the “edit” button. You will welcome the update environment page. Also, click on the icons to reach the developer portal website, identify server, and endpoint gateway pages.

.. image:: ../docs/images/EnvironmentDetails_Image.png
  :width: 425
  :alt: apigo environment detail

For more information about how to add an environment, click on.

.. _click on: https://docs.apigo.com/en/latest/HowToAddanEnvironment.html

Update Environment
~~~~~~~~~~~~~~~~~~

Click on "Details of Environment" to edit existed ones. To update environments, click on the “edit” button. The environment name and description can be rewritten with this page.  

.. image:: ../docs/images/UpdateEnv_Image.png
  :width: 425
  :alt: apigo update environment 

You can change the status of the environment to active or not.

.. image:: ../docs/images/UpdateEnv_Image2.png
  :width: 425
  :alt: apigo environment status

.. meta::
  :description: Visit our page to learn about ApiGo environments, review our document and discover the privileges of the ApiGo Open Banking platform!