======
v1.2.1
======

Creation Date: 10.08.2020

`Send Mail Policy`_
-------------------

.. _Send Mail Policy: https://apigo.com/admin/endpoints

**8769 Send Mail Policy Development**

With the send mail policy Tenant admin can notify anyone via sending an e-mail when a request is received by the gateway. Thus, with ApiGo, the ability to control requests received over the gateway by sending an e-mail feature has been added.

•	Mail addresses can be added.
•	Mail subject and content can be dynamically managed.
•	SMTP setting can be done over the Configuration settings.

Endpoint Identification
-----------------------

**9506 Adding description for APIs**

The API definition can be added by Tenant Admin on the Endpoint definition screen and viewed in the documents. Thus, ApiGo users can add more detailed API descriptions in Swagger.

•	Description field is optional.
•	The entered value can be seen as an explanation in the Swagger Documentation Page.

Dynamic Model Management
------------------------

**8638 Error Log Mapping Settings**

Tenant admin can determine which model to map the errors that will occur on the destination points. Thus, the ApiGo user can create and manage their own error standard through the Gateway.

•	It can be managed over a single console.
•	Error model can be specified on this page.
•	Destination error model can be entered.
•	Matching can be done.
•	Error responses in the gateway are made according to this model.

`Developer Portal Login Methods`_
---------------------------------

.. _Developer Portal Login Methods: https://apigo.com/admin/site-settings

**8641 Developer Portal Login with Google**

Tenant admin enables developers to log into the Developer Portal with their Google accounts by making the configurations on the Management Portal.

•	Located in Developer Portal Settings.
•	This setting can be configurate for environments without mTLS authentication.
•	It is in the login and register section of the Developer Portal.

**8642 Developer Portal Login with GitHub**

Tenant admin enables developers to log into the Developer Portal with their GitHub accounts by making the configurations on the Management Portal.

•	Located in Developer Portal Settings.
•	This setting can be configurate for environments without mTLS authentication.
•	It is in the login and register section of the Developer Portal.

Hosting and Domain Management
-----------------------------

**4147 WAF Implementation**

ApiGo users can work in a more secure structure with the WAF system located on ApiGo Gateway.

•	WAF infrastructure has been established.
•	Gateway is compatible with WAF.

FIXED ISSUES
------------

.. csv-table:: FIXED ISSUES
   :header: PORTAL, TASK, ISSUE, ACTION, STATUS
   :widths: 1, 1, 3, 1, 1
    
   Management Portal, 10146, The slider is crashed in the Management Portal mobile view., UI Fixed, Done
   Management Portal, 10147, Clicking on the description of a record in the Gateway Log file breaks the detail part., UI Fixed, Done
   Management Portal, 10339, The Application Endpoints Documents and Developers card links in the dashboard cannot go to the relevant destination., UI Fixed, Done
   Management Portal, 10354, Certificate Chain error is responded in Prod environments of apigo.com and apigo.com.tr, Certificate Added, Done
   Management Portal, 10576, Cache error is responded in Prod and Test environments., UI Fixed, Done
   Developer Portal, 10308, Documentation pages cannot be viewed when browsing other than Chrome., UI Fixed, Done
   Gateway, 10081, ApiGo Gateway X-Real-IP cannot come properly., Traefik Configuration Fixed, Done
   
.. meta::
  :description: Visit our page to get detailed information about the ApiGo Open Banking platform v1.2.1 version and to review the innovations!