=========
`v1.4.6`_
=========

.. _v1.4.6: https://apigo.com/admin/

Creation Date: 17.11.2020

`User Account Management`_
--------------------------

.. _User Account Management: https://apigo.com/admin/users

**17101 - Improvements to record user scope preferences during the token acquisition process**

During the token acquisition process, the scope preferences of PSU are recorded. This provides the necessary regulative solutions for banks operating in Turkey.

•	Preferences which include creation date, tenant, and token details on the Identity Server Scope selection screen are saved.

`User Guide of ApiGo`_
----------------------

.. _User Guide of ApiGo: https://docs.apigo.com/

**10428 - Documentation - How to define an endpoint?**

A document has been prepared for how tenant admin can define an endpoint on Management Portal to provide the highest user experience level.

•	Explanation of how to create a token depends on authentication type.
•	Explanation of how to define an endpoint with GET method and client credential token.
•	Explanation of how to define an endpoint with POST method and authorization code.
•	Explanation of how to define query strings to an endpoint.

`Performance Improvements of ApiGo`_
------------------------------------

.. _Performance Improvements of ApiGo: https://apigo.com

**15911 – Infrastructure improvements to monitor ApiGo**

ApiGo functionality can be monitored from end to end, thus the infrastructure has been improved to increase customer satisfaction and ensure product continuity.

•	Service interruptions can be notified to the relevant person from the ApiGo team automatically as an alarm.
•	The availability is regularly followed by ApiGo team to provide a better customer experience.

**15547 - Evaluating performance metrics and performance improvements for ApiGo**

By determining the maximum level of load that can be met, scalability is defined. 

•	Load tests have been executed.
•	Necessary improvements have been defined and the product performance have been empowered.

FIXED ISSUES
------------

.. csv-table:: FIXED ISSUES
   :header: PORTAL, TASK, ISSUE, ACTION, STATUS
   :widths: 1, 1, 3, 1, 1

   Management Portal, 18148, There is no control for the previously defined tenant admin via mail on the user management page., UI fixed, Done
   Management Portal and Gateway, 15556, An application that is disabled on the Management Portal can make traffic through the gateway., Backend fixed , Done
   Management Portal, 15568, Using a number in the name label causes errors when defining a new environment., UI fixed , Done
   Management Portal and Identity Server, 15844, The logo cannot be seen on the Management Portal and Identity Server scope screen after the bank logo is loaded., UI fixed , Done
   Developer Portal, 13427, The application list screen has a problem with the size and location of components., UI fixed , Done
   Management Portal, 15923, There is a Turkish language problem on the Management Portal Forgot Password page., UI fixed , Done
   Management Portal, 13425, A login method can be added on the Social Login page without checking the required field., UI fixed , Done
   Management Portal, 13358, The settings cannot be displayed properly in Turkish when going to the account update page., UI fixed , Done
   Developer Portal, 13422, Removing the “Register” button on the Swapper for the environments created with mTLS Authentication, UI fixed , Done
   Management Portal, 16787, The “Change my Password” option is available for a user who is registered with social login., UI fixed , Done
   Management Portal, 15924, A user registered on the Developer Portal can change the password on the Management Portal., UI fixed , Done
   Management Portal, 15223, Not coming new environment data on some screens when the environment is changed., UI fixed , Done
   Management Portal, 16860, The verification link sent to the user via e-mail does not work after adding a new tenant admin., UI and Backend fixed , Done
   Management Portal, 15940, A user defined as Endpoint admin cannot update the developer details., UI and Backend fixed , Done
   Management Portal, 18225, Endpoint admin gets an error on the token management page., UI fixed , Done
   Management Portal, 17103, The status of expired tokens is listed as “Live” on the Token Management page., UI fixed , Done
   Management Portal, 13375, The option to select HTTP and HTTPS in multifunction policy causes an error., UI fixed , Done
   Management Portal, 15222, There is a Turkish character problem in the Export-Import feature., UI fixed , Done

.. meta::
  :description: Visit our page to get detailed information about the ApiGo Open Banking platform v1.4.6 version and to review the innovations!