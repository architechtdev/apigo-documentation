================
Account Settings
================

To change your account settings, you can click on the bank icon in the upper right corner. In the opened list, you can `manage your bank settings`_, `change your password`_, or sign out.

.. _change your password: https://apigo.com/admin/change-password

.. _manage your bank settings: https://apigo.com/admin/bank

.. image:: ../docs/images/AccountSettings.PNG
    :alt: apigo account settings

You can change your current password with a new one. Please fill in the relevant blanks to change your password and click on the "Save" button.

.. image:: ../docs/images/ChangePassword.PNG
    :alt: apigo change password

If you want to change your bank's country or SWIFT number, you can use these settings. To change the details, you need the fill the relevant blanks and click on the "Save" button.

.. image:: ../docs/images/Bank Settings.PNG
    :alt: apigo bank settings

Also, you can use sign out with this function. But, don't forget that ApiGo Team hope you do not need it forever.

.. meta::
   :description: Visit our page to learn about ApiGo account settings, review our document and discover the privileges of the ApiGo Open Banking platform!