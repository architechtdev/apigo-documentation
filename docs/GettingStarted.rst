===============
Getting Started
===============

ApiGo enables you to manage all critical aspects of PSD2 with a central token providing you an exclusive customer experience with a single fully secure API on a cloud-hosted platform.

.. _ApiGo: https://apigo.com/

* Third-Party Program (TPP) Management 
* Consent Management 
* DDoS (Distributed Denial-of-Service) Protection 
* Centralized Token 
* Monitoring and Reporting Endpoint Usages

Quick start video
-----------------

Get PSD2 complied quickly saving money and time! We offer a SaaS solution that solves the difficult PSD2 compliance task for banks enabling new business models and opportunities via open banking.

This screencast will help you get started.

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/dXZPOB1DHhY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. meta::
  :description: Visit our page to learn how to get started with ApiGo, review our documents and discover the privileges of the ApiGo Open Banking system!