===============
`Quota Management`_
===============

Quota management can be defined as using a given number of API resources in a particular period. By applying a quota in your API management, 
you make sure how much the load on your systems can be. This way, you can plan by knowing what the maximum number of resources you will 
dedicate to the real systems behind the API ends.

Quota management and Rate Limit (throttling) are often known as the same thing. However, Rate Limit defines how many requests can be made to 
an API in a short time like 1 second and how much time is needed for a new request to be accepted, but quota management defines how much 
access can be made to an API resource in a wide period. The issue of how much an API resource can be accessed within defined periods such 
as one day, one week, one month or one year and which message will be returned until the next period after this expiration has been addressed 
to the Quota management.

**The ApiGo and Quota Management**

Unlike competing products, ApiGo can manage quota in 3 different ways. 

*	API Based quota management
*	Application (Client Application) based quota management
*	Domain Based quota management